from django.urls import path, include
from . import views

app_name = "login"
urlpatterns = [
    path('',  include('django.contrib.auth.urls')),
    path('loginok/', views.after_login, name='after_login'),
]