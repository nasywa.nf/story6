from django.test import TestCase, Client

# Create your tests here.
class unitTest(TestCase):
    def test_url_exist(self):
        response = self.client.get('/login/login/')
        self.assertEqual(response.status_code, 200)

    def test_url_to_html(self):
        response = self.client.get('/login/login/')
        self.assertTemplateUsed(response, 'registration/login.html')
    
    def test_url_not_exist(self):
        response = Client().get('/hai')
        self.assertEqual(response.status_code, 404) 