from django.forms import ModelForm
from .models import StatusModels

class StatusModelsForms(ModelForm):
    class Meta:
        model = StatusModels
        fields = [
            'status',
        ]