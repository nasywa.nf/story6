(function() {
    
$('.toggle').click(function(e) {
    e.preventDefault();

  var $this = $(this);

  if ($this.next().hasClass('show')) {
      $this.next().removeClass('show');
      $this.next().slideUp(350);
  } else {
      $this.parent().parent().find('.inner').removeClass('show');
      $this.parent().parent().find('.inner').slideUp(350);
      $this.next().toggleClass('show');
      $this.next().slideToggle(350);
  }
});
//var checkbox = document.querySelector('input[name=theme]')[0];

$('input[name=theme]').click(function() {
    if($(this).is(':checked')) {
        $('body').attr('data-theme', 'biru')
    } else {
        $('body').attr('data-theme', 'merahmuda')
    }
})

// let trans = () => {
//     document.documentElement.classList.add('transition');
//     window.setTimeout(() => {
//         document.documentElement.classList.remove('transition')
//     }, 1000)
// }

})()