from django.shortcuts import render
from .forms import StatusModelsForms
from .models import StatusModels
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect

# Create your views here.
def status_view(request):    
    response = {}
    form = StatusModelsForms(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        mod = StatusModels()
        mod.status = form.cleaned_data['status']
        mod.save()

        context = {
            'form':form,
            'all_mod':StatusModels.objects.all(),
        }
        return render(request,"status.html",context)
    else:
        form = StatusModelsForms(request.POST or None)
        context = {
            'form':form,
            'all_mod':StatusModels.objects.all(),
        }

        return render(request,"status.html",context)

    


# Create your views here.
