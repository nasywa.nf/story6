from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.storage import staticfiles_storage 
from django.contrib.staticfiles import finders
from .models import StatusModels
from .forms import StatusModelsForms
from .views import status_view
from django.http import HttpRequest
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import time

class unitTest(TestCase):
    def test_url_exist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_to_html(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'status.html')
    
    def test_url_fires_correct_methods(self):
        response = resolve('/')
        self.assertEqual(response.func,status_view)

    def test_status_masuk_gak(self):
        response = self.client.post('/', data={'status' : 'nasywa'})
        self.assertEqual(StatusModels.objects.count(), 1)
    
    def test_isi_form_kosong(self):
        objeknya = StatusModels()
        response = self.client.post(objeknya)
        self.assertEqual(StatusModels.objects.all().count(),0)

class functionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='./chromedriver')
    
    def test_input_todo(self):
        self.driver.get(self.live_server_url)
        self.driver.find_element_by_id('id_status').send_keys('haiiiiiinya 4 kali')
        time.sleep(5)
        self.driver.find_element_by_name('button_submit').click()
        time.sleep(5)
        html_response = self.driver.page_source
        self.assertIn('haiiiiiinya 4 kali', html_response)

    def test_change_theme(self):
        self.driver.get(self.live_server_url)
        theme_button = self.driver.find_element_by_id('switch')
        self.assertNotIn('data-theme="biru"', self.driver.page_source)
        self.driver.execute_script("arguments[0].click();", theme_button)
        self.assertIn('data-theme="biru"', self.driver.page_source)

    def test_accordion(self):
        self.driver.get(self.live_server_url)
        self.assertIn('class="accordion"', self.driver.page_source)
    
    def tearDown(self):
        self.driver.quit()
        super().tearDown()





        
        



# Create your tests here.
