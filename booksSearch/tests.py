from django.test import TestCase
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve

# Create your tests here.
class unitTest(TestCase):
    def test_url_exist(self):
        response = self.client.get('/books/')
        self.assertEqual(response.status_code, 200)

    def test_url_to_html(self):
        response = self.client.get('/books/')
        self.assertTemplateUsed(response, 'books.html')
    
    def test_url_not_exist(self):
        response = Client().get('/hai')
        self.assertEqual(response.status_code, 404) 
  