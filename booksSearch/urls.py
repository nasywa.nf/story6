from django.urls import path
from . import views

app_name = "booksSearch"
urlpatterns = [
    path('', views.book_page, name='books_page'),
    path('data/', views.books, name='books_data'),
]
